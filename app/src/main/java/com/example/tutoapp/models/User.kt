package com.example.tutoapp.models

class User (username:String = "", userMail:String = "" , userTel:String = "", image:String = "")
{
    private var username:String = username
    private var usermail:String = userMail
    private var userTel:String = userTel
    private var image:String = image

    public fun getUsername():String{
        return this.username
    }
    public fun getUserMail():String{
        return this.usermail
    }
    public fun getUserTel():String{
        return this.userTel
    }
    public fun getImage():String{
        return this.image
    }

    //setters
    public fun setUsername(username:String){
        this.username = username
    }
    public fun setUserMail(userMail:String){
        this.usermail = usermail
    }
    public fun setUserTel(userTel:String){
        this.userTel = userTel
    }
    public fun setImage(image:String){
        this.image = image
    }

}