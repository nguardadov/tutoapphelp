package com.example.tutoapp.connections

import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseUser
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.storage.FirebaseStorage
import com.google.firebase.storage.StorageReference

class Connection {

    private var auth: FirebaseAuth? = null
   

    init {
        this.auth = FirebaseAuth.getInstance() // extrellendo el usuario seleccionado
        
    }

    public fun getCurrentUser() : FirebaseUser? {
        return this.auth?.currentUser
    }


    public  fun getInstance(): FirebaseDatabase {
        return FirebaseDatabase.getInstance()  //FirebaseStorage.getInstance()
    }


    public fun getSotrage(): StorageReference {
        return  FirebaseStorage.getInstance().reference
    }

    public fun getInstanceStorage(): FirebaseStorage {
        return  FirebaseStorage.getInstance()
    }


}