package com.example.tutoapp.method

import android.content.Context
import android.net.Uri
import android.widget.Toast
import com.example.tutoapp.PerfilFragment
import com.example.tutoapp.connections.Connection
import com.example.tutoapp.models.User
import com.google.firebase.auth.FirebaseUser
import com.google.firebase.database.FirebaseDatabase
import java.util.*

class Dao {

    companion object {
        const val collectionUser:String = "Users"
        const val urlImage:String = "urlImage"
        const val directory:String = "images/"
    }


    //funcion para agregar la imagen
    private fun addUserImage(url:String,currentUser:FirebaseUser?, instancia: FirebaseDatabase){
        //agregaremos el dato al usuario logueado
        val user:FirebaseUser?=currentUser
        val ref = instancia.getReference(collectionUser)
        val userRef = ref.child(user?.uid!!)

        //con esto agremanos la ruta
        userRef.child(urlImage).setValue(url)
    }


    public fun uploadImage(image:Uri, context: Context){

        var cnx = Connection()

        val uuid = UUID.randomUUID()


        val imageName = directory+"$uuid.${image.lastPathSegment}" // nombre que se le dara a la imagen

        println(imageName)

        var storageReference = cnx.getSotrage()!!.child(imageName)

        storageReference.putFile(image!!)
            .addOnSuccessListener {
                val newReference = cnx.getInstanceStorage().getReference(imageName) // la instancia del storage

                newReference.downloadUrl
                    .addOnSuccessListener { uri->

                        //agrenado el campo url al usuario
                        addUserImage(uri.toString(),cnx.getCurrentUser(),cnx.getInstance())
                        //imageUser.setImageURI(uri.)
                    }
            }
            .addOnFailureListener { exception ->
                if (exception != null) {
                    Toast.makeText(context, exception.localizedMessage, Toast.LENGTH_LONG).show()
                }
            }
            .addOnCompleteListener { task ->
                if (task.isComplete) {
                    Toast.makeText(context, "Imagen agregada con exito!", Toast.LENGTH_LONG).show()

                    // intent
                }
            }

    }


}